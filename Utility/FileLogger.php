<?php
//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.

//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

namespace Phaln\Utility;

use Phaln\Utility\Utilities;

/**
 * Description of FileLogger
 *
 * @author phaln
 * @license http://cecill.info/index.fr.html CeCILL-C
 */
class FileLogger {

    public static $defaultFileName = LOG_DIR . 'phaln.log';

    /**
     * Pemert d'écrire le Throwable dans un fichier de log
     * @param \Throwable $ex l'erreur à logger
     * @return string|null le hash md5 de l'erreur loggée (inscrit également dans le fichier)
     */
    public static function TraceError(\Throwable $ex): ?string {
	$datas = [
	    'url' => Utilities::getUrl(),
	    'fileName' => $ex->getFile(),
	    'line' => $ex->getLine(),
	    'message' => $ex->getMessage(),
	    'infos' => [
		'codeException' => $ex->getCode(),
		'stackTrace' => $ex->getTraceAsString(),
	    ],
	];
	dump_var($datas, DUMP, 'Datas à logger:');
	return self::Trace($datas);
    }

    /**
     * Pemert d'écrire un tableau d'informations dans un fichier de log
     * @example 
     * $logDatas['date','url','fileName','line','message','infos'=>['codeException','stackTrace',],]
     * @param array $datas les données à logger
     * @return string|null le hash md5 de l'erreur loggée (inscrit également dans le fichier)
     * @throws \Exception en cas d'accès impossible au fichier
     */
    public static function Trace(array $datas): ?string {
	$str = '';
	$code = -1;

	if (isset($datas['logFileName'])) {
	    $fileName = $datas['logFileName'];
	    FileLogger::$defaultFileName = $fileName;
	} else {
	    $fileName = FileLogger::$defaultFileName;
	}

	$logDatas['date'] = date('Y-m-d H:i:s');
	$logDatas['url'] = (isset($datas['url'])) ? $datas['url'] : Utilities::getUrl();
	$logDatas['fileName'] = (isset($datas['fileName'])) ? $datas['fileName'] : '';
	$logDatas['line'] = (isset($datas['line'])) ? $datas['line'] : '';
	$logDatas['message'] = (isset($datas['message'])) ? $datas['message'] : '';
	$logDatas['infos'] = (isset($datas['infos'])) ? $datas['infos'] : null;

	foreach ($logDatas as $key => $value) {
	    if ($key != 'infos') {
		$str .= $value . ';';
	    } else {
		foreach ($value as $key => $info) {
		    $str .= $key . '=' . $info . ';';
		}
	    }
	}

	if ($fd = fopen($fileName, 'a+')) {
	    if (flock($fd, LOCK_EX)) {
		$code = md5($str);
		$strf = $code . ';' . $str . PHP_EOL;
		if (false === fwrite($fd, $strf)) {
		    throw new \Exception("Erreur d'écriture dans le fichier de log $fileName");
		}
		flock($fd, LOCK_UN);    // Enlève le verrou
	    } else {
		throw new \Exception("Impossible de verrouiller le fichier $fileName");
	    }

	    fclose($fd);
	} else {
	    throw new \Exception("Erreur d'ouverture du fichier de log $fileName");
	}

	return $code;
    }

}
