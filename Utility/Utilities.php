<?php

//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.
//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

namespace Phaln\Utility;

/**
 * Divers fonction utiles
 *
 * @author phaln
 */
final class Utilities
{

    /**
     * Récupère depuis $_SERVER l'url.
     * 
     * Attention, nécessaire pour le filtrage de $_SERVER qui pose problème sur certaines installation Linux...
     * @return string
     */
    public static function getUrl(): string {
        //  Exemple d'URL:http://cni.phaln.info/services/api.obj.cni-uwpdemo.php/Mesures/Capteur/4
        if (filter_has_var(INPUT_SERVER, "REQUEST_URI")) {
            $requestUri = filter_input(INPUT_SERVER, "REQUEST_URI", FILTER_VALIDATE_URL, FILTER_NULL_ON_FAILURE);
        } else {
            if (isset($_SERVER["REQUEST_URI"])) {
                $requestUri = filter_var($_SERVER["REQUEST_URI"], FILTER_VALIDATE_URL, FILTER_NULL_ON_FAILURE);
            } else {
                $requestUri = null;
            }
        }
        dump_var($requestUri, DEBUG_PHALN, 'Utilities, $requestUri:');
        return $requestUri;
    }

    /**
     * Récupère depuis $_SERVER le fichier correspndant à l'url.
     * 
     * Attention, nécessaire pour le filtrage de $_SERVER qui pose problème sur certaines installation Linux...
     * @return string
     */
    public static function getScriptName(): string {
        if (filter_has_var(INPUT_SERVER, "SCRIPT_NAME")) {
            $scriptName = filter_input(INPUT_SERVER, "SCRIPT_NAME", FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
        } else {
            if (isset($_SERVER["SCRIPT_NAME"])) {
                $scriptName = filter_var($_SERVER["SCRIPT_NAME"], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
            } else {
                $scriptName = null;
            }
        }
        dump_var($scriptName, DEBUG_PHALN, 'Utilities, $scriptName:');
        return $scriptName;
    }

    /**
     * Récupère depuis $_SERVER la méthode HTTP.
     * 
     * Attention, nécessaire pour le filtrage de $_SERVER qui pose problème sur certaines installation Linux...
     * @return string
     */
    public static function getRequestMethod(): string {
        if (filter_has_var(INPUT_SERVER, "REQUEST_METHOD")) {
            $requestMethod = filter_input(INPUT_SERVER, "REQUEST_METHOD", FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
        } else {
            if (isset($_SERVER["REQUEST_METHOD"])) {
                $requestMethod = filter_var($_SERVER["REQUEST_METHOD"], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
            } else {
                $requestMethod = null;
            }
        }
        dump_var($requestMethod, DEBUG_PHALN, 'Utilities, REQUEST_METHOD:');
        return $requestMethod;
    }

    /**
     * Récupère depuis $_SERVER le REFERER.
     * 
     * Attention, nécessaire pour le filtrage de $_SERVER qui pose problème sur certaines installation Linux...
     * @return string
     */
    public static function getReferer(): string {
        if (filter_has_var(INPUT_SERVER, "HTTP_REFERER")) {
            $res = filter_input(INPUT_SERVER, "HTTP_REFERER", FILTER_VALIDATE_URL, FILTER_NULL_ON_FAILURE);
        } else {
            if (isset($_SERVER["HTTP_REFERER"])) {
                $res = filter_var($_SERVER["HTTP_REFERER"], FILTER_VALIDATE_URL, FILTER_NULL_ON_FAILURE);
            } else {
                $res = null;
            }
        }
        dump_var($res, DEBUG_PHALN, 'Utilities, HTTP_REFERER:');
        return $res;
    }

    /**
     * Fonction de remplacement des caractères accentués dans une chaîne. Fonctionne en utf-8.
     * Utilise des RegEx.
     * http://www.weirdog.com/blog/php/supprimer-les-accents-des-caracteres-accentues.html
     * @param type $str la chaîne à traiter
     * @param type $charset le charset à utiliser
     * @return string la chaîne traitée
     */
    public static function wd_remove_accents(string $str, string $charset = 'utf-8'): string {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);

        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

        return $str;
    }

}
