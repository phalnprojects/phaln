<?php
//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.

//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

//  Nécessaire pour éviter le "bug" du json_encode qui travaille différement
//  avec les float (pb de précision) à partir de PHP7.1
if (version_compare(phpversion(), '7.1', '>=')) {
    ini_set('serialize_precision', -1);
}


// Permet de définir la constante DEBUG_PHALN si elle n'est pas définie dans la config de l'application
if (!defined('DEBUG_PHALN')) {
    define('DEBUG_PHALN', false);
}

// Permet de définir la constante DUMP si elle n'est pas définie dans la config de l'application
if (!defined('SHOW_EXCEPTIONS')) {
    if(defined('DUMP_EXCEPTIONS')) {
	define('SHOW_EXCEPTIONS', DUMP_EXCEPTIONS);
    } else {
	define('SHOW_EXCEPTIONS', false);
    }
}

// Permet de définir la constante DUMP si elle n'est pas définie dans la config de l'application
if (!defined('DUMP')) {
    define('DUMP', false);
}

// Si la constante LOG_DIR n'est pas définie, elle est positionnée à dossier_racine/log
if (!defined('LOG_DIR')) {
    define('LOG_DIR', dirname(__FILE__, 5) . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR);
}

// Si la constante URL_BASE n'est pas définie, elle est positionnée à l'url de démo
if (!defined('URL_BASE')) {
    define('URL_BASE', 'https://demophalntwig.phaln.info');
}

//  Active ou pas l'affichage de debug et les var_dump
if (DEBUG_PHALN || SHOW_EXCEPTIONS) {
    error_reporting(E_ALL | E_STRICT);
    ini_set('display_errors', 'On');
} else {
    error_reporting(E_ALL);
    ini_set('display_errors', 'Off');
    ini_set('log_errors', 'On');
    ini_set('error_log', LOG_DIR . 'phaln_log.txt');
}


//  Regarde si le paquet var_dumper de Symfony est installé pour ajuster la version de dump_var à utiliser
if (!defined('DUMP_MODE')) {
    if (function_exists('dump')) {
	define('DUMP_MODE', 'dump');
    } else {
	define('DUMP_MODE', 'phaln');
    }
}

/**
 * Fonction à utiliser de préférence à var_dump() surtout lorsque XDebug n'est pas déployé sur la configuration.
 * 
 * Attention, pour voir les affichages, il faut:
 * 	* soit basculer la constante DUMP à TRUE dans votre application...
 * 	* soit passer TRUE en deuxième paramètre.
 * 
 * Vous pouvez passer en 3ème paramètre une chaîne de caractères pour commenter le dump.
 * Dans ce cas, vous DEVEZ donner une valeur pour le deuxième paramètre (soit DUMP, soit TRUE).
 * @param mixed $var la variable à afficher
 * @param bool $dump soit DUMP pour utiliser la configuration, soit true pour afficher tout le temps (pas en prod!)
 * @param string $msg le message d'en-tête de l'affichage du dump.
 */
function dump_var($var, $dump = DUMP, $msg = null) {
    if ($dump) {
	switch (DUMP_MODE) {
	    case 'phaln':
		if ($msg) {
		    echo"<p><strong>$msg</strong></p>";
		}
		echo '<pre>';
		var_dump($var);
		echo '</pre>';
		break;
	    case 'dump':
		if ($msg) {
		    echo"<p><strong>$msg</strong></p>";
		}
		dump($var);
		break;
	    default:
		var_dump($var);
	}
    }
}
