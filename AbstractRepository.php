<?php

//Copyright Philippe ALLUIN (2020)
//contributeur :
//- Luc Gilot
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.
//Copyright Philippe ALLUIN (2020)
//contributor(s) : 
//- Luc Gilot 
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

namespace Phaln;

use Phaln\BDD;
use Phaln\Exceptions\RepositoryException;

/**
 * Une abstraction permettant de mutualiser les méthodes communes à la plupart des repositories.
 * Elle vous permet de déclarer facilement vos propres repositories
 * en surchangeant le constructeur pour affecter les attributs.
 * Pour rappel, un repository permet de faire le mapping relationel
 * entre une table de la BDD et une classe entité correspondante.
 * Ici, les entités manipulées héritent de Phaln\AbstractEntity et
 * seuls leurs attributs protected sont gérés.
 *
 * @author phaln, Luc Gilot et Sonny Chaprier
 * 
 * @license http://cecill.info/index.fr.html CeCILL-C
 */
abstract class AbstractRepository
{

    /**
     * la base de donnée (PDO)
     * @var PDO
     */
    protected $db = null;

    /**
     * le nom de la table manipulée
     * @var string
     */
    protected $table = '';

    /**
     * Le nom de la classe mappée par ce repository, de préférence son nom complet avec l'espace de nom.
     * @var string
     */
    protected $classMapped = '';

    /**
     * le nom du ou des champ(s) clé(s) primaire(s).
     * En cas de clés multiples, c'est un tableau.
     * Par défaut, clé primaire unique nommée id.
     * @example 'id_etudiant'
     * @example ['id_etudiant', 'id_promotion']
     * @var mixed
     */
    protected $idFieldName = 'id';

    /**
     * Précise si la pk unique est autoincrémentée.
     * True par défaut.
     * @var bool
     */
    protected $isPkAutoIncremented = true;

    /**
     * Constructeur.
     * 
     * Lors de la surcharge par votre propre repository, vous devez appeler celui-ci 
     * et initialiser les attributs $table, $classMapped, $idFieldName et $isPkAutoIncremented (si != de true).
     * 
     * Récupère la connexion PDO à la bdd pour initialiser l'attribut $db.
     * @use Phaln\BDD
     * @throws RepositoryException
     */
    public function __construct() {
	$this->db = BDD::getConnexion();
	if (!$this->db) {
	    throw new \Phaln\Exceptions\RepositoryException('Pb d\'accès à db dans AbstractRepository::_construct()');
	}
    }

    //
    //	Méthodes protégées utilitaires.
    //

    /**
     * Exécute une requête préparée de sélection pour rendre soit une entité, soit une colelction d'entités.
     * @used-by getAll(), getById(), getBy()
     * @param type $reqPrep une requête préparée (mais pas encore exécutée)
     * @return \Phaln\AbstractEntity un objet de classe classMapped dérivant AbstractEntity ou une colelction d'objets de classe classMapped dérivant AbstractEntity ou `null` si aucun enregistrement.
     * @throws RepositoryException
     */
    protected function getByReqPrep(\PDOStatement $reqPrep) {
	$resultSet = null;  //  Le résultat de l'exécution
	//  Exécution de la requête préparée
	$rqtResult = $reqPrep->execute();
	dump_var($rqtResult, DEBUG_PHALN, 'AbstractRepository::getByReqPrep() $rqtResult:');

	if ($rqtResult) {
	    //  Exécution correcte
	    $reqPrep->setFetchMode(\PDO::FETCH_ASSOC);
	    $nbRep = $reqPrep->rowCount();
	    dump_var($nbRep, DEBUG_PHALN, 'AbstractRepository::getByReqPrep() $nbRep:');
	    switch ($nbRep) {
		case 0:
		    //  Pas de résultat, $resultSet reste null
		    break;
		case 1:
		    //  Un seul résultat, $resultSet sera une instance de type 'entité $classMapped
		    $resultSet = new $this->classMapped($reqPrep->fetch());
		    break;
		default:
		    // Il y en a plus que 1...
		    // $resulSet sera un tableau d'instances de type $classMapped
		    foreach ($reqPrep as $row) {
			$resultSet[] = new $this->classMapped($row);
		    }
		    break;
	    }
	} else {
	    //  Exécution incorrecte
	    dump_var($reqPrep->errorInfo(), DEBUG_PHALN, 'PDOStatement::errorInfo():');
	    throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::getByReqPrep(): ' . $reqPrep->errorInfo()[2]);
	}
	return $resultSet;
    }

    /**
     * Construit le contenu d'une clause WHERE pour les clés primaires
     * @param type $id
     * @return string le contenu de la clause WHERE (attention, sans WHERE...)
     * @throws RepositoryException
     */
    protected function whereForId($id): string {
	$where = '';

	if (is_array($this->idFieldName) && is_array($id)) {
	    // On a une clé primaire multiple
	    // On vérifie si on a bien le même nombre de clé entre $id et $idFieldName
	    $pkDiff = array_diff($this->idFieldName, array_keys($id));
	    dump_var($pkDiff, DEBUG_PHALN, 'AbstractRepository::getById() $pkDiff:');

	    if (count($pkDiff) == 0) {
		//  Pas de différence...
		//  Composition de clause WHERE
		foreach ($this->idFieldName as $keyField) {
		    if ($where) {
			$where .= ' AND ';
		    }
		    $where .= $keyField . ' = :' . $keyField;
		}
	    } else {
		throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::getById(): nombre d\'éléments différent dans la liste des champs clés primaires');
	    }
	} else {
	    if (!is_array($this->idFieldName) && !is_array($id)) {
		//  On a une clé primaire unique
		$where = $this->idFieldName . ' = :' . $this->idFieldName;
	    } else {
		throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::getById(): nombre d\'éléments différent dans la liste des champs clés primaires');
	    }
	}
	return $where;
    }

    /**
     * Réalise le binding des clés primaires sur la requête préparée.
     * @param \PDOStatement $reqPrep la requête préparée à binder
     * @param type $id les pk à binder
     */
    protected function bindForId(\PDOStatement $reqPrep, $id) {
	if (is_array($this->idFieldName)) {
	    foreach ($this->idFieldName as $keyField) {
		$reqPrep->bindValue(':' . $keyField, $id[$keyField]);
	    }
	} else {
	    $reqPrep->bindValue((':' . $this->idFieldName), $id);
	}
    }

    /**
     * Retrourne la ou les valeurs de clé(s) primaire(s) de l'entité
     * @param \Phaln\AbstractEntity $entity l'entité dont on veut les pk
     * @return mixed la valeur ou le tableau de valeur
     */
    protected function getPkValue(AbstractEntity $entity) {
	$pk = null;

	if (is_array($this->idFieldName)) {
	    //  On a une clé primaire multiple
	    $entityTab = $entity->__toArray();
	    foreach ($this->idFieldName as $value) {
		if (is_null($entityTab[$value])) {
		    $pk = null;
		    break;
		} else {
		    $pk[$value] = $entityTab[$value];
		}
	    }
	} else {
	    //  Clé primaire unique
	    //  !! Attention, au nom du getter qui dépend des conventions de nommage.
	    $get = 'get' . $this->idFieldName;
	    $pk = $entity->$get();
	}
	dump_var($pk, DEBUG_PHALN, 'AbstractRepository::getPkvalue() $pk');

	return $pk;
    }

    /**
     * Réalise l'insertion d'une nouvelle entité dans la bdd.
     * @param \Phaln\AbstractEntity $entity
     * @return \Phaln\AbstractEntity l'entitée insérée avec sa pk à jour (en cas d'autoincrémentation) ou null
     * @throws RepositoryException
     */
    protected function insert(AbstractEntity $entity): ?AbstractEntity {
	dump_var($entity, DEBUG_PHALN, 'AbstractRepository::insert $entity');
	$resultSet = null;

	//  extraction sous forme de tableau des attributs protected de l'entité.
	$columnToSave = $entity->__toArray();
	//  Supprime les colonnes dont les valeurs sont nulles
	foreach ($columnToSave as $key => $value) {
	    if (is_null($value)) {
		unset($columnToSave[$key]);
	    }
	}
	dump_var($columnToSave, DEBUG_PHALN, 'AbstractRepository::insert() $columnToSave');

	//  Mise en chaîne de caractères des colonnes à sauver et binder
	$columnToSaveString = '';
	$columnToBindString = '';
	//  Le tableau pour le binding, les clés sont les tag à binder
	$columnToBind = [];
	foreach ($columnToSave as $columnName => $columnValue) {
	    $columnToSaveString .= $columnName . ',';
	    $columnToBindString .= ':' . $columnName . ',';
	    $columnToBind[':' . $columnName] = $columnValue;
	}
	//  Suppression de la ',' finale dans les string
	$columnToSaveString = substr($columnToSaveString, 0, -1);
	$columnToBindString = substr($columnToBindString, 0, -1);
	dump_var($columnToBind, DEBUG_PHALN, 'AbstractRepository::insert() $columnToBind');

	// Requête SQL
	$query = 'INSERT INTO ' . $this->table
		. ' (' . $columnToSaveString . ')'
		. ' VALUES (' . $columnToBindString . ')';
	dump_var($query, DEBUG_PHALN, 'AbstractRepository::insert() Requête SQL:');

	//  Préparation et exécution de la requête
	$reqPrep = $this->db->prepare($query);
	dump_var($reqPrep, DEBUG_PHALN, 'AbstractRepository::insert() $reqPrep');
	$res = $reqPrep->execute($columnToBind);
	dump_var($reqPrep, DEBUG_PHALN, 'AbstractRepository::insert() $reqPrep');

	if ($res) {
	    if (!is_array($this->idFieldName)) {
		$setter = 'set' . $this->idFieldName;
		$entity->$setter($this->db->lastInsertId());
	    }
	    $resultSet = $entity;
	} else {
	    //  Exécution incorrecte
	    dump_var($reqPrep->errorInfo(), DEBUG_PHALN, 'PDOStatement::errorInfo():');
	    throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::insert(): ' . $reqPrep->errorInfo()[2]);
	}

	return $resultSet;
    }

    /**
     * Met à jour (update) une entité
     * @param \Phaln\AbstractEntity $entity
     * @return \Phaln\AbstractEntity l'entitée mise à jour ou null
     * @throws RepositoryException
     */
    protected function update(AbstractEntity $entity): ?AbstractEntity {
	dump_var($entity, DEBUG_PHALN, 'AbstractRepository::update $entity');
	$resultSet = null;

	//  extraction sous forme de tableau des attributs protected de l'entité.
	$columnToUpdate = $entity->__toArray();
	//  Supprime les colonnes dont les valeurs sont nulles et les clés primaires
	foreach ($columnToUpdate as $key => $value) {
	    if (is_array($this->idFieldName)) {
		if ((array_search($key, $this->idFieldName) !== false)) {
		    unset($columnToUpdate[$key]);
		}
	    } else {
		if ($key === $this->idFieldName) {
		    unset($columnToUpdate[$key]);
		}
	    }
	}
	dump_var($columnToUpdate, DEBUG_PHALN, 'AbstractRepository::update() $columnToUpdate');

	//  Le tableau pour le binding
	$columnToBind = [];
	foreach ($entity->__toArray() as $columnName => $columnValue) {
	    $columnToBind[':' . $columnName] = $columnValue;
	}
	dump_var($columnToBind, DEBUG_PHALN, 'AbstractRepository::update() $columnToBind');

	//  Mise en chaîne de caractères des colonnes à updater et binder
	$columnToUpdateString = '';
	foreach ($columnToUpdate as $columnName => $columnValue) {
	    $columnToUpdateString .= $columnName . ' = :' . $columnName . ',';
	}
	//  Suppression de la ',' finale dans les string
	$columnToUpdateString = substr($columnToUpdateString, 0, -1);

	//  Composition de la requete SQL
	$query = 'UPDATE ' . $this->table
		. ' SET ' . $columnToUpdateString
		. ' WHERE ' . $this->whereForId($this->getPkValue($entity));

	//  Préparation et exécution de la requête
	$reqPrep = $this->db->prepare($query);
	dump_var($reqPrep, DEBUG_PHALN, 'AbstractRepository::update() $reqPrep');
	$res = $reqPrep->execute($columnToBind);
	dump_var($reqPrep, DEBUG_PHALN, 'AbstractRepository::update() $reqPrep');

	if ($res) {
	    $resultSet = $entity;
	} else {
	    //  Exécution incorrecte
	    dump_var($reqPrep->errorInfo(), DEBUG_PHALN, 'PDOStatement::errorInfo():');
	    throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::insert(): ' . $reqPrep->errorInfo()[2]);
	}

	return $resultSet;
    }

    //
    //	Les méthodes publiques utilisables
    //

    /**
     * Récupère tous les enregistrements de la table, éventuellement trié (voir paramètre).
     * @param array|null $orderBy Tableau pour les classements. De la forme ['prenom' => 'ASC', 'nom' => 'DESC']
     * @return array|null un tableau d'objet de classe classMapped dérivant AbstractEntity
     */
    public function getAll(?array $orderBy = null): ?array {
	$resultSet = null;
	$orderString = '';
	dump_var($orderBy, DEBUG_PHALN, 'AbstractRepository::getAll() $orderBy:');

	$query = 'SELECT * FROM ' . $this->table;
	if (!is_null($orderBy)) {
	    $orderString = ' ORDER BY ';
	    $i = 1;
	    foreach ($orderBy as $field => $order) {
		if ($i++ > 1) {
		    $orderString .= ', ';
		}
		$orderString .= $field . ' ' . $order;
	    }
	    $query .= $orderString;
	}
	dump_var($query, DEBUG_PHALN, 'AbstractRepository::getAll() Requête SQL:');

	//  Préparation et exécution de la requête
	$reqPrep = $this->db->prepare($query);
	$resultSet = $this->getByReqPrep($reqPrep);

	if (is_null($resultSet)) {
	    $resultSet = [];
	} else {
	    if (!is_array($resultSet)) {
		$resultSet = array($resultSet);
	    }
	}
	return $resultSet;
    }

    /**
     * Récupère l'enregistrement n° $id dans la table.
     * 
     * $id peut être une valeur unique (clé primaire simple) ou un tableau
     * en cas de clés primaires multiples.
     * @use whereForId()
     * @param type $id la valeur de la pk à récupérer (éventuellement un tableau)
     * @return \Phaln\AbstractEntity un objet de classe classMapped dérivant AbstractEntity
     */
    public function getById($id) {
	$resultSet = null;

	$where = $this->whereForId($id);
	//  La requête
	$query = 'SELECT * FROM ' . $this->table .
		' WHERE ' . $where;
	dump_var($query, DEBUG_PHALN, 'AbstractRepository::getById() Requête SQL:');

	//  Préparation et binding
	$reqPrep = $this->db->prepare($query);

	// binding soit avec le tableau de clés, soit avec la clé unique
	$this->bindForId($reqPrep, $id);
	//  Exécution de la requête préparée.
	$resultSet = $this->getByReqPrep($reqPrep);
	dump_var($resultSet, DEBUG_PHALN, 'AbstractRepository::getById() $resultSet:');

	return $resultSet;
    }

    /**
     * Sélectionne les entités d'une table selon les critères du tableau de tableau(x) byTab, 
     * éventuellement trié selon le deuxième paramètre.
     * 
     * Il est possible de sélectionner selon plusieurs champs ('fieldName')
     * et d'utiliser des opérateurs de comparaison ('comp') comme:
     * 
     * 	    - '='
     * 	    - 'LIKE'
     * 	    - ...
     * 
     * La clé 'value' est celle du champ. On peut y utiliser les jockers.
     * 
     * On peut décider d'avoir plusieurs critères de sélection, la clé 'op'
     * permet de préciser commen sont liés les critères ('AND', 'OR', ...).
     * 
     * @example 
     * ```
     * [
     * 	    ['fieldName' => 'test', 'comp'=>'=', 'value'=>'a tester'],
     * 	    ['op'=>'AND', 'fieldName' => 'test2', 'comp'=>'LIKE', 'value'=>'rien'],
     * ]
     * ```
     * @example 
     * ```
     * 	    "SELECT * FROM maTable WHERE test='a tester' AND test2 LIKE 'rien'"
     * ```
     * 
     * @param array $byTab 
     * 	    Le tableau de critères (chaque critère est un tableau) pour composer la clause WHERE
     * @param array|null $orderBy 
     * 	    Le tableau pour les classements. De la forme `['prenom' => 'ASC', 'nom' => 'DESC']`
     * @return array|null 
     * 	    Un tableau d'objet de classe `classMapped` dérivant `AbstractEntity`
     */
    public function getBy(array $byTab, ?array $orderBy = null): ?array {
	$resultSet = null;
	$orderString = '';

	//  Composition de la clause WHERE de la requête
	$where = '';
	foreach ($byTab as $by) {
	    $where .= (isset($by['op'])) ? ' ' . $by['op'] . ' ' : '';
	    $where .= $by['fieldName'] . ' ' . $by['comp'] . ' :' . $by['fieldName'];
	}

	//  La requête
	$query = 'SELECT * FROM ' . $this->table .
		' WHERE ' . $where;

	if (!is_null($orderBy)) {
	    $orderString = ' ORDER BY ';
	    $i = 1;
	    foreach ($orderBy as $field => $order) {
		if ($i++ > 1) {
		    $orderString .= ', ';
		}
		$orderString .= $field . ' ' . $order;
	    }
	    $query .= $orderString;
	}
	dump_var($query, DEBUG_PHALN, 'AbstractRepository::getBy() Requête SQL:');

	//  Préparation et binding
	$reqPrep = $this->db->prepare($query);
	foreach ($byTab as $by) {
	    $reqPrep->bindValue(':' . $by['fieldName'], $by['value']);
	}

	// Exécution de la requête préparée.
	$resultSet = $this->getByReqPrep($reqPrep);

	if (is_null($resultSet)) {
	    $resultSet = [];
	} else {
	    if (!is_array($resultSet)) {
		$resultSet = array($resultSet);
	    }
	}
	return $resultSet;
    }

    /**
     * Efface l'enregistrement n° $id dans la table
     * @param type $id la valeur de la pk à supprimer
     * @return bool True si ok, false sinon
     * @throws RepositoryException
     */
    public function deleteById($id): bool {
	$resultset = false;

	//  Composition de la requête SQL
	$where = $this->whereForId($id);
	$query = 'DELETE FROM ' . $this->table .
		' WHERE ' . $where;
	dump_var($query, DEBUG_PHALN, 'Requête SQL:');

	// Préparation, binding et exécution
	$reqPrep = $this->db->prepare($query);
	$this->bindForId($reqPrep, $id);
	$rqtResult = $reqPrep->execute();
	dump_var($rqtResult, DEBUG_PHALN, 'AbstractRepository::deleteById() $rqtResult:');

	if ($rqtResult) {
	    if (is_null($this->getById($id))) {
		$resultset = true;
	    }
	} else {
	    dump_var($reqPrep->errorInfo(), DEBUG_PHALN, 'PDOStatement::errorInfo():');
	    throw new \Phaln\Exceptions\RepositoryException('Pb db dans AbstractRepository::deleteById(): ' . $reqPrep->errorInfo()[2]);
	}
	return $resultset;
    }

    /**
     * Efface l'entité de la table
     * @param \Phaln\AbstractEntity $entity l'entité à supprimer
     * @return bool True si ok, false sinon
     */
    public function deleteEntity(AbstractEntity $entity): bool {
	//  On compose la pk utilisée pour utiliser le deleteById
	$pk = $this->getPkValue($entity);
	if (!is_null($pk)) {
	    return $this->deleteById($pk);
	} else {
	    return false;
	}
    }

    /**
     * Enregistre (insert ou update) une entité dans la bdd
     * @param \Phaln\AbstractEntity $entity
     * @return \Phaln\AbstractEntity|null
     * @throws RepositoryException
     */
    public function save(AbstractEntity $entity): ?AbstractEntity {
	dump_var($entity, DEBUG_PHALN, 'AbstractRepository::save $entity');
	$resultSet = null;

	if ($this->exist($entity)) {
	    // id not null et entité existante dans bdd, donc update
	    $resultSet = $this->update($entity);
	} else {
	    // id not null et entité inexistante dans bdd, donc insert
	    $resultSet = $this->insert($entity);
	}

	return $resultSet;
    }

    /**
     * Recherche si l'enregistrement correspondant à $entity existe dans la bdd.
     * @param \Phaln\AbstractEntity $entity l'entité à rechercher
     * @return bool TRUE si existe
     */
    public function exist(AbstractEntity $entity): bool {
	$result = false;

	// préparation de la clé primaire pour la recherche avec getById()
	$pk = $this->getPkValue($entity);

	//  Recherche
	if (is_null($pk)) {
	    $result = false;
	} else {
	    $ent = $this->getById($pk);
	    if (is_null($ent)) {
		$result = false;
	    } else {
		$result = true;
	    }
	}
	return $result;
    }

    //
    //	Méthodes dépréciées présentes pour la compatibilité avec la V1
    //

    /**
     * @deprecated since version 2.0
     * @see save()
     * Fonction (à surcharger dans V1) pour assurer l'enregistrement dans la bdd.
     * 
     * Fonction directement implémentée dans V2, sa surcharge est devenue inutile.
     * 
     * Fonction conservée pour compatibilité
     * @param \Phaln\AbstractEntity $entity
     */
    public function sauver(AbstractEntity $entity) {
	return $this->save($entity);
    }

    /**
     * @deprecated since version 2.0
     * @see getById()
     * Récupère l'enregistrement n° $id dans la table. 
     * 
     * Fonction conservée pour compatibilité.
     * @param type $id la valeur de la pk à récupérer
     * @return \Phaln\AbstractEntity un objet de classe classMapped dérivant AbstractEntity
     */
    public function getEntityId($id) {
	return $this->getById($id);
    }

    /**
     * @deprecated since version 2.0
     * @see getById()
     * Récupère l'enregistrement n° $id dans la table.
     * 
     * Fonction conservée pour compatibilité
     * @param type $id la valeur de la pk à récupérer
     * @return \Phaln\AbstractEntity un objet de classe classMapped dérivant AbstractEntity
     */
    public function getEntityById($id) {
	return $this->getById($id);
    }

    /**
     * @deprecated since version 2.0
     * @see getById()
     * Récupère l'enregistrement n° $id dans la table.
     * 
     * Fonction conservée pour compatibilité.
     * @param type $id la valeur de la pk à récupérer
     * @return \Phaln\AbstractEntity un objet de classe classMapped dérivant AbstractEntity
     */
    public function getId($id) {
	return $this->getById($id);
    }

    /**
     * @deprecated since version 2.0
     * @see deleteById()
     * Efface l'enregistrement n° $id dans la table.
     * 
     * @param type $id la valeur de la pk à récupérer.
     * @return bool True si ok, false sinon
     */
    public function deleteEntityId($id) {
	return $this->deleteById($id);
    }

}
