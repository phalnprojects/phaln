<?php
//Copyright Philippe ALLUIN (2020)
//contributeur :
//- Luc Gilot
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.

//Copyright Philippe ALLUIN (2020)
//contributor(s) : 
//- Luc Gilot 
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.


namespace Phaln;

/**
 * Classe conservant les instances de repositories utilisées pour éviter les instanciations multiples.
 *
 * @author phaln
 * 
 * @version 1.2: ajout d'attributs pour les espaces de noms des entités et des repositories
 * 
 * @license http://cecill.info/index.fr.html CeCILL-C
 */
class Manager {

    /**
     * L'espace de noms de base par défaut dans lequel se trouve les espaces Repositories et Entities
     * @var string
     */
    private static $defaultModelNameSpace = 'Model';
    
    /**
     * L'espace de nom par défaut contenant les entités
     * @var string 
     */
    private static $defaultEntitiesNameSpace = 'Entities';
    
    /**
     * L'espace de nom par défaut contenant les repositories
     * @var string 
     */
    private static $defaultRepositoriesNameSpace = 'Repositories';

    /**
     * Le tableau des repositories
     * @var Array
     */
    private static $repositories = array();

    /**
     * Récupère (ou instancie) le repository de l'entité en paramètre
     * @param string $entityName le nom de l'entité.
     * Il peut être complet (avec espaces de nom) ou simple pour utiliser les attributs.
     * @param string|null $otherBaseNameSpace Vous pouvez préciser l'espace de nom de base, si différent de celui en attribut
     * @return \Phaln\AbstractRepository le repository de l'entité
     * @throws Exceptions\ManagerException
     */
    public static function getRepository(string $entityName, ?string $otherBaseNameSpace = null): AbstractRepository {
	dump_var(self::$repositories, DEBUG_PHALN, 'Les repositories existant:');
	// On peut éventuellement avoir des entités dans d'autres espace de nom de base que celui par défaut.
	$saveEntityName = (class_exists($entityName))
				? ($entityName) 
				: ((!is_null($otherBaseNameSpace)) 
					? ($otherBaseNameSpace . '\\' . self::$defaultEntitiesNameSpace . '\\' . $entityName )
					: (self::$defaultModelNameSpace . '\\' . self::$defaultEntitiesNameSpace . '\\' . $entityName)
				);
	dump_var($saveEntityName, DEBUG_PHALN, 'Le nom complet de l\'entité:');

	if (!isset(self::$repositories[$saveEntityName])) {
	    //  S'il n'y a pas de repository pour cette entité dans le tableau, on l'instancie
	    $repoName = (!is_null($otherBaseNameSpace))
			    ? ($otherBaseNameSpace . '\\' . self::$defaultRepositoriesNameSpace . '\\' . $entityName . 'Repository')
			    : (self::$defaultModelNameSpace . '\\' . self::$defaultRepositoriesNameSpace . '\\' . $entityName . 'Repository');
	    dump_var($repoName, DEBUG_PHALN, 'Le nom complet du repository:');
	    if (class_exists($repoName) && class_exists($saveEntityName)) {
		self::$repositories[$saveEntityName] = new $repoName();
	    } else {
		throw new Exceptions\ManagerException("$repoName inexistant.");
	    }
	}
	return self::$repositories[$saveEntityName];
    }

    /**
     * Lecteur pour $defaultAppNameSpace
     * @return string $defaultAppNameSpace
     */
    public static function getDefaultModelNameSpace() :string {
	return self::$defaultModelNameSpace;
    }

    /**
     * Lecteur pour $defaultEntitiesNameSpace
     * @return string $defaultEntitiesNameSpace
     */
    public static function getDefaultEntitiesNameSpace() :string {
	return self::$defaultEntitiesNameSpace;
    }

    /**
     * Lecteur pour $defaultRepositoriesNameSpace
     * @return string $defaultRepositoriesNameSpace
     */
    public static function getDefaultRepositoriesNameSpace() :string {
	return self::$defaultRepositoriesNameSpace;
    }

    /**
     * Affecte le nom de l'espace de nom, relatif à celui de base, pour les entities.
     * Entities par dégaut.
     * @param string $defaultEntitiesNameSpace
     * @return void
     */
    public static function setDefaultEntitiesNameSpace(string $defaultEntitiesNameSpace): void {
	self::$defaultEntitiesNameSpace = $defaultEntitiesNameSpace;
    }

    /**
     * Affecte le nom de l'espace de nom, relatif à celui de base, pour les repositories.
     * Repositories par dégaut.
     * @param string $defaultRepositoriesNameSpace
     * @return void
     */
    public static function setDefaultRepositoriesNameSpace(string $defaultRepositoriesNameSpace): void {
	self::$defaultRepositoriesNameSpace = $defaultRepositoriesNameSpace;
    }

    /**
     * Affecte le nom de l'espace de nom de base pour l'application dans lequel
     * se trouvent les espaces de nom pour les entités et les repositories.
     * App par dégaut.
     * @param string $appNameSpace
     * @return void
     */
    public static function setDefaultModelNameSpace(string $appNameSpace): void {
	self::$defaultModelNameSpace = $appNameSpace;
    }

}
