<?php
//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.

//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

namespace Phaln;

use Phaln\Exceptions\BDDException;
use \PDO;

/**
 * Classe encapsulant sous forme de singleton une connexion PDO à la base de données.
 * Elle ne peut pas être dérivée.
 * 
 * @author phaln
 * 
 * @license http://cecill.info/index.fr.html CeCILL-C
 */
final class BDD {

    /**
     * L'instance singleton
     * @var BDD
     */
    private static ?BDD $_bdd = null;

    /**
     * La connexion PDO
     * @var PDO
     */
    private ?PDO $connexion;

    /**
     * Les informations de la base de données. Voir les fichiers /config/appConfig.php
     * @var array
     */
    public static $infoBdd; //

    /**
     * 	Constructeur privé, singleton oblige...
     */
    private function __construct() {
	$db = null;

	try {
	    dump_var(BDD::$infoBdd, DEBUG_PHALN, 'infoBdd: ');
	    $mydriver = (isset(BDD::$infoBdd['type'])) ? strtolower(BDD::$infoBdd['type']) : 'mysql';
	    $myport = (isset(BDD::$infoBdd['port'])) ? BDD::$infoBdd['port'] : (($mydriver === 'mysql') ? 3306 : 5432);
	    $mycharset = (isset(BDD::$infoBdd['charset'])) ? BDD::$infoBdd['charset'] : 'UTF8';
	    $hostname = (isset(BDD::$infoBdd['host'])) ? BDD::$infoBdd['host'] : "localhost";
	    $mydbname = (isset(BDD::$infoBdd['dbname'])) ? BDD::$infoBdd['dbname'] : null;
	    $myusername = (isset(BDD::$infoBdd['user'])) ? BDD::$infoBdd['user'] : null;
	    $mypassword = (isset(BDD::$infoBdd['pass'])) ? BDD::$infoBdd['pass'] : '';

	    if ($hostname == null || $mydbname == null || $myusername == null) {
		throw new BDDException('Il manque des informations essentielles à BDD::$infoBdd[].');
	    }
	    $connect_error = '';
	    try {
		//  Composition du DSN
		$dsn = "$mydriver:dbname=$mydbname;host=$hostname;port=$myport";
		dump_var($dsn, DEBUG_PHALN, 'DSN: ');
		switch ($mydriver) {
		    case 'mysql':
			$dsn .= ';charset=' . $mycharset;
			break;
		    case 'pgsql':
			$dsn .= ';options=\'--client_encoding=' . $mycharset . '\'';
			break;
		}
		$db = new PDO($dsn, $myusername, $mypassword);
		if ($db) {
		    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $this->connexion = $db;
		} else {
		    throw new Exceptions\BDDException('Instanciation Bdd impossible');
		}
	    } catch (\PDOException $e) {
		$connect_error = $e->getMessage();
		if (DEBUG_PHALN) {
		    echo 'PDOException dans BDD: ' . $connect_error . '<br/>';
		}
		$ex = new Exceptions\BDDException('Instanciation Bdd impossible <br/>' . $connect_error . '<br/>' . $e->getTraceAsString());
		throw $ex;
	    }
	    dump_var($this->connexion, DEBUG_PHALN, 'Connexion BDD Ok: ');
	}// fin try
	catch (\Throwable $ex) {
	    $myEx = new Exceptions\BDDException($ex->getMessage() . ' ' . $ex->getFile());
	    throw $myEx;
	}
    }

    /**
     * L'accès à l'instance unique.
     * @return BDD l'instance singleton.
     */
    public static function get_bdd(): self {
	// Si l'instance n'existe pas, on exécute le constructeur pour la créer
	if (is_null(self::$_bdd)) {
	    self::$_bdd = new self;
	}

	//  On retourne l'instance existante, qui vient d'être créée ou qui existait déjà.
	return self::$_bdd;
    }

    /**
     *  __clone vide pour s'assurer de ne pas pouvoir créer de copie du singleton
     */
    private function __clone() {
	
    }

    /**
     * Lecteur de la connexion à la bdd (PDO)
     * @return PDO l'objet accèdant à la base de données.
     */
    public function get_connexion(): ?PDO {
	return $this->connexion;
    }

    /**
     * Lecteur statique de la connexion à la bdd (PDO)
     * @return PDO l'objet accèdant à la base de données.
     */
    public static function getConnexion(): ?PDO {
	return self::get_bdd()->connexion;
    }

}
