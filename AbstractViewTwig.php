<?php
//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.

//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

namespace Phaln;

use Phaln\Utility\Utilities;

/**
 * Classe abstraite encapsulant le chargement du template twig.
 *
 * @author phaln
 * 
 * @license http://cecill.info/index.fr.html CeCILL-C
 */
abstract class AbstractViewTwig
{

    /**
     * Le chargeur pour Twig
     * @var \Twig\Loader\FilesystemLoader
     */
    protected $loader;
    
    /**
     * Le "moteur" Twig lui-même
     * @var \Twig\Environment
     */
    protected $twig;
    
    /**
     * Le template chargé et à rendre.
     * @var type
     */
    protected $template;

    /**
     * Initialise le loader et le template à rendre
     * Ajoute l'extension DebugExtension si la constante DEBUG est true.
     * Création des variables globales twig:
     * 'debug', 'urlBase', 'publicDir', 'session', 'requestMethod'
     * @param string $templateFile
     */
    public function __construct(string $templateFile)
    {
        dump_var($templateFile, DEBUG_PHALN, "TemplateFile dans AbstractVueTwig:");
        $this->loader = new \Twig\Loader\FilesystemLoader(TEMPLATE_PATH);
    
        //  Récupération du chargeur et ajout du composant pour le dump des varaibles Twig
        if (defined('DUMP')) {
            $this->twig = new \Twig\Environment($this->loader, ['debug' => DUMP,]);
            $this->twig->addExtension(new \Twig\Extension\DebugExtension());
            $this->twig->addGlobal('dump', DUMP);
        } else {
            $this->twig = new \Twig\Environment($this->loader);
        }
    
        //  Démmarage du moteur et initialisation des variables globales
        if (defined('URL_BASE')) {
            $this->twig->addGlobal('urlBase', URL_BASE);
        }
        if (defined('PUBLIC_DIR')) {
            $this->twig->addGlobal('publicDir', PUBLIC_DIR);
        }
        $this->twig->addGlobal('session', $_SESSION);
    
        //  Récupération de REQUEST_METHOD et déclaration de la variable globale
//        if (filter_has_var(INPUT_SERVER, "REQUEST_METHOD")) {
//            $method = filter_input(INPUT_SERVER, "REQUEST_METHOD", FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
//        } else {
//            if (isset($_SERVER["REQUEST_METHOD"])) {
//                $method = filter_var($_SERVER["REQUEST_METHOD"], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
//            } else {
//                $method = null;
//            }
//        }
        $method = Utilities::getRequestMethod();
        $this->twig->addGlobal('requestMethod', $method);

    
        //  Chargement du template
        $this->template = $this->twig->load($templateFile);
    }

    /**
     * Effectue le rendu du template.
     */
    abstract public function render(array $datas);
}
