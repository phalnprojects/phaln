<?php
//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.

//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

namespace Phaln;

use Phaln\Exceptions as Ex;
use Phaln\Utility\Utilities;

/**
 * Analyse l'URL et gère les routes.
 *  
 * Les routes sont définies dans /config/routes.json
 * 
 * La classe est implémentée en singleton.
 * 
 * @author phaln
 * 
 * @license http://cecill.info/index.fr.html CeCILL-C
 */
class Router {

    /**
     * Le tableau des routes.
     * @var array 
     */
    protected array $routes = [];
    
    /**
     * L'instance unique du Router.
     * @var Router 
     */
    private static ?Router $router = null;
    
    /**
     * Le nom du fichier json contenant les routes.
     * 
     * Voir config/exemples/routes.json pour le format du fichier.
     * @var string 
     */
    public static ?string $routesFile = null;

    /** 
     * Constructeur privé (Singleton... Utilisez Router::get()).
     * 
     * Décode le fichier json pour construire le tableau des routes.
     * 
     */
    private function __construct() {
	$this->routes = json_decode(Router::$routesFile, true);
	dump_var($this->routes, DEBUG_PHALN, 'Router, Les routes:');
    }

    /**
     *  __clone vide pour s'assurer de ne pas pouvoir créer de copie du singleton
     */
    private function __clone() {
	
    }

    /**
     * Récupère depuis $_SERVER l'url.
     * 
     * Attention, nécessaire pour le filtrage de $_SERVER qui pose problème sur certaines installation Linux...
     * @return string
     */
    private function getUrl() :string {
	//  Exemple d'URL:http://cni.phaln.info/services/api.obj.cni-uwpdemo.php/Mesures/Capteur/4
	if (filter_has_var(INPUT_SERVER, "REQUEST_URI")) {
	    $requestUri = filter_input(INPUT_SERVER, "REQUEST_URI", FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
	} else {
	    if (isset($_SERVER["REQUEST_URI"])) {
		$requestUri = filter_var($_SERVER["REQUEST_URI"], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
	    } else {
		$requestUri = null;
	    }
	}
	dump_var($requestUri, DEBUG_PHALN, 'Router, $requestUri:');
	return $requestUri;
    }

    /**
     * Récupère depuis $_SERVER le fichier correspndant à l'url.
     * 
     * Attention, nécessaire pour le filtrage de $_SERVER qui pose problème sur certaines installation Linux...
     * @return string
     */
    private function getScriptName() :string{
	if (filter_has_var(INPUT_SERVER, "SCRIPT_NAME")) {
	    $scriptName = filter_input(INPUT_SERVER, "SCRIPT_NAME", FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
	} else {
	    if (isset($_SERVER["SCRIPT_NAME"])) {
		$scriptName = filter_var($_SERVER["SCRIPT_NAME"], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
	    } else {
		$scriptName = null;
	    }
	}
	dump_var($scriptName, DEBUG_PHALN, 'Router, $scriptName:');
	return $scriptName;
    }

    /**
     * L'accès à l'instance unique.
     * @param string|null $routesFile Le nom du fichier de routes à utiliser, si différent de celui par défaut.
     * @return Router l'instance singleton.
     * @throws Ex\RouterException Si pas de fichier de route disponible.
     */
    public static function get(?string $routesFile = null) {
	//  Affectation du routeFile
	if (!is_null($routesFile) && $routesFile != '') {
	    self::$routesFile = $routesFile;
	} else {
	    if (defined(CONFIG_DIR)) {
		//  prend celui par défaut dans le dossier config de l'application.
		if (file_exists(CONFIG_DIR . 'routes.json')) {
		    self::$routesFile = CONFIG_DIR . 'routes.json';
		}
	    }
	    else {
		throw new Ex\RouterException('Pas de routes fournies, ni de fichier routes.json.');
	    }
	}

	if (is_null(self::$routesFile)) {
	    throw new Ex\RouterException('Problème avec le fichier des routes.');
	}


	// Si l'instance n'existe pas, on exécute le constructeur pour la créer
	if (is_null(self::$router)) {
	    self::$router = new self;
	}
	dump_var(self::$router, DEBUG_PHALN, '$router dans Router::get()');

	//  On retourne l'instance existante, qui vient d'être créée ou qui existait déjà.
	return self::$router;
    }

    /**
     * Effectue le routage de l'url vers l'action du contrôleur selon le fichier routes.json
     * @throws \Phaln\Exceptions\RouterException
     */
    public function Route() {
	$ctrlName = '';
	$actionName = '';
	$params = array();

	//  Route reçue en get
	$routeName = ($tmp = filter_input(INPUT_GET, 'rte', FILTER_SANITIZE_SPECIAL_CHARS)) ? $tmp : '/';
	if ($tmp) {
	    array_shift($_GET);
	}

	dump_var($routeName, DEBUG_PHALN, 'Router, $routeName:');

	//  La route existe dans le tableau des routes?
	if (array_key_exists($routeName, $this->routes)) {
	    $params = array(); //  Les éventuels paramètres à transmettre à l'action
	    //  Récupération de tous les paramètres éventuels, qu'ils soient en GET ou en POST
	    //  pour les rassembler dans le tableau $params
  
	    //  Par défaut, ceux en GET sont tous validés en string.
	    //  @todo amélioration possible en définisasnt dans le fichier route le type des paramètres.
	    foreach ($_GET as $key => $value) {
		$params[$key] = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
	    }
	    
	    // Ceux en POST ne sont pas filtrés (normalement, utilisation des Validators...)
	    foreach ($_POST as $key => $value) {
		$params[$key] = $value;
	    }
	    dump_var($params, DEBUG_PHALN, 'Router, $params:');

	    //  Vérification de l'existance du controller
	    $ctrlName = $this->routes[$routeName]['controller'];
	    if (!class_exists($ctrlName)) {
		throw new \Phaln\Exceptions\RouterException("Le controller '$ctrlName' n'est pas valide");
	    }
	    $controller = new $ctrlName();

	    //  Vérification de l'existence de l'action dans le controller
	    $actionName = $this->routes[$routeName]['action'];
	    if (!method_exists($controller, $actionName)) {
		throw new \Phaln\Exceptions\RouterException("La méthode '$ctrlName:$actionName' n'est pas valide");
	    }
	    $controller->$actionName($params);
	} else {
	    throw new \Phaln\Exceptions\RouterException('Route inconnue...');
	}
    }

}
