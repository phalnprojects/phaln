<?php
//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.

//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

namespace Phaln;

/**
 * Classe abstraite AbstractEntity encapsulant les accesseurs en lecture et écriture
 * par défaut aux attributs d'une classe.
 * 
 * N'a de sens que pour les classes de la couche modèle qui ne contiennent
 * que des attributs et leurs accesseurs, sans méthode "complexe".
 *
 * ATTENTION!!
 * 
 * Seuls les attributs protected de la classe filles peuvent êtres manipulés par
 * les méthodes de la classe AbstractEntity.
 * 
 * Si vous ajoutez des attributs privés, vous devez proposer les accesseurs et
 * surcharger les méthodes nécessaires, à minima __construct() et hydrate()
 *
 * @author Phaln
 * 
 * @license http://cecill.info/index.fr.html CeCILL-C
 */
abstract class AbstractEntity implements \JsonSerializable {

    /**
     * Constructeur, se contente d'appeller la méthode d'hydratation.
     * @param array $datas le tableau associatif des attributs à affecter
     */
    public function __construct(array $datas = null) {
	$this->hydrate($datas);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut protected.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs protected à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = null) {
	//  Récupère la liste des attributs protected de la classe à laquelle appartient l'objet concret
	$attrib = get_class_vars(get_class($this));

	// Appelle le mutateur des attributs existant dans le tableau $datas
	foreach ($attrib as $key => $val) {
	    // Appeller le mutateur correspondant à la clé étudiée, selon sa casse
	    if (isset($datas[$key])) {
		$mutateur = 'set' . $key;
		$this->$mutateur($datas[$key]);
	    } elseif (isset($datas[strtolower($key)])) {
		$key = strtolower($key);
		$mutateur = 'set' . $key;
		$this->$mutateur($datas[$key]);
	    } elseif (isset($datas[strtoupper($key)])) {
		$key = strtoupper($key);
		$mutateur = 'set' . $key;
		$this->$mutateur($datas[$key]);
	    }
	}

	return $this;
    }

    /**
     * Mutateur simple d'accès en écriture à un attribut protected.
     * Peut être surchargé.
     * 
     * ATTENTION!! ne fonctionne pas sur un attribut private!!
     * @param type $attribut le nom de l'attribut à modifier
     * @param type $valeur la nouvelle valeur de l'attribut
     * @return \AbstractEntity l'objet courant pour faciliter le chaînnage des set.
     */
    protected function set($attribut, $valeur) {
	$this->$attribut = $valeur;
	return $this;
    }

    /**
     * Lecteur simple d'accés en lecture à un attribut protected
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut
     * @return type la valeur de l'attribut
     */
    protected function get($attribut) {
	return $this->$attribut;
    }

    /**
     * Méthode "magique" interceptant l'appel de méthode et cherchant à exécuter
     * les accesseurs.
     * @param type $methode nom complet de la méthode
     * @param type $attribValeur la valeur passée en paramètre à la méthode
     * @return type le résultat de l'exécution
     */
    public function __call($methode, $attribValeur) {
	$attribName = null;

	//  Le préfixe, normalement get ou set
	$prefix = substr($methode, 0, 3);

	//  Le suffixe, normalement un nom d'attribut
	$suffix = substr($methode, 3);

	//  Le nombre de valeur à affecter à l'attribut,
	//  normalement 0 pour un get et 1 pour un set
	$cattrs = count($attribValeur);

	//  Vérification de l'existence de l'attribut correspondant au suffixe.
	//  La première lettre du suffixe peut être en majuscule ou non.
	if (property_exists($this, $suffix)) {
	    $attribName = $suffix;
	} elseif (property_exists($this, lcfirst($suffix))) {
	    $attribName = lcfirst($suffix);
	}

	if ($attribName == null) {
	    // Tenter de trouver la propriété même avec une autre casse
	    $laProps = $this->__toArray();
	    foreach ($laProps as $key => $values) {
		if (strtolower($key) == strtolower($suffix)) {
		    // Cette propriété est bien celle qu'on cherche
		    $attribName = $key;
		    break;
		}
	    }
	}

	//  Il existe bien un attribut correspondant au suffixe
	if ($attribName != null) {
	    if ($prefix == 'set' && $cattrs == 1) {
		return $this->set($attribName, $attribValeur[0]);
	    }
	    if ($prefix == 'get' && $cattrs == 0) {
		return $this->get($attribName);
	    }
	} else {
	    throw new Exceptions\EntityException("La méthode $methode n'existe pas...");
	}
    }

    /**
     * La classe implémente l'interface JsonSerializable ce qui permet
     * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
     * @return mixed le tableau pour conversion en json
     */
    public function jsonSerialize(): mixed {
	$array = array();

	//  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
	$attrib = get_class_vars(get_class($this));

	// Associe la clé du nom de l'attribut à la valeur de cet attribut
	foreach ($attrib as $key => $val) {
	    $array[$key] = $this->get($key);
	}
	return $array;
    }

    /**
     * Pour affichage simple, style csv, d'un objet
     * @return string une string composée des valeurs des attributs séparés par un ';'
     */
    public function __toString() : string {
	$string = "";

	//  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
	$attrib = get_class_vars(get_class($this));

	// Ajoute la valeur de chaque attribut à la $string
	foreach ($attrib as $key => $val) {
	    $string .= $this->get($key) . ';';
	}

	return $string;
    }

    /**
     * Transforme un objet en un tableau associatif.
     * @return array tableau associatif des attributs => valeurs
     */
    public function __toArray() : mixed {
	return $this->jsonSerialize();
    }

}
