<?php
//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.

//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

namespace Phaln;

/**
 * AbstractValidator permet de définir des règles de validation basiques pour les entités
 *
 * @author phaln
 * 
 * @version 1.2 Ajout de validEntityFactory()
 * 
 * @license http://cecill.info/index.fr.html CeCILL-C
 */
abstract class AbstractValidator
{

    /**
     * Contient les filtres PHP à appliquer aux attributs des entités.
     * Voir https://www.php.net/manual/fr/filter.filters.php
     * Tableau associatif: les clés sont les noms des attributs et les valeurs les filtres
     * @var array
     */
    protected $attributFilters = array();
    
    /**
     * Contient les tailles des champs/attributs (dans le cas de string) conforment avec la bdd
     * @var array
     */
    protected $attributSizes = array();
    
    /**
     * Contient les attributs qui ne peuvent pas être null.
     * @var array
     */
    protected $attributRequired = array();

    /**
     * Affecte le tableau des tailles
     * @param array $sizes
     * @return $this
     */
    public function setAttributSizes(array $sizes) :self
    {
        $this->attributSizes = $sizes;
        return $this;
    }
    
    /**
     * Retourne le tableau des tailles
     * @return array
     */
    public function getAttributSizes()
    {
        return $this->attributSizes;
    }
    
    /**
     * Affecte le tableau des filtres
     * @param array $filters
     * @return $this
     */
    public function setAttributFilters(array $filters) :self
    {
        $this->attributFilters = $filters;
        return $this;
    }
    
    /**
     * Retourne le tableau des filtres
     * @return array
     */
    public function getAttributFilters()
    {
        return $this->attributFilters;
    }
    
    /**
     * Affecte le tableau des attributs non-nullable
     * @param array $filters
     * @return $this
     */
    public function setAttributRequired(array $required) : self
    {
        $this->attributRequired = $required;
        return $this;
    }
    
    /**
     * Retourne le tableau des attributs non-nullable
     * @return array
     */
    public function getAttributRequired()
    {
        return $this->attributRequired;
    }
    
    /**
     * Ajoute un élément au tableau des filtres
     * @param string $attributName le nom de l'attribut
     * @param int $filterName le filtre (FILTER_xxx_xxx) à appliquer sur l'attribut
     * @return $this
     */
    public function addAttributFilters(string $attributName, int $filterName = FILTER_DEFAULT ) :self
    {
        $this->attributFilters[$attributName] = $filterName;
        return $this;
    }
    
    /**
     * Ajoute un élément au tableau des tailles
     * @param string $attributName le nom de l'attribut
     * @param int $size sa longueur max
     * @return $this
     */
    public function addAttributSize(string $attributName, int $size) :self
    {
        $this->attributSizes[$attributName] = $size;
        return $this;
    }
    
    
    /**
     * Ajoute un élément au tableau des attributs non-nullable
     * @param string $attributName le nom de l'attribut
     * @param bool $required
     * @return $this
     */
    public function addAttributRequired(string $attributName, bool $required = true)
    {
	if($required)
	    $this->attributSizes[$attributName] = 'required';
        return $this;
    }
    /**
     * Constructeur.
     * @param array $validators Tableau associatif avec les clés 'Filters', 'Sizes' et 'Required' qui sont les tableaux à affecter aux attributs.
     */
    public function __construct(array $validators = null)
    {
        if (isset($validators['Filters'])) {
            $this->setAttributFilters($validators['Filters']);
        }
        if (isset($validators['Sizes'])) {
            $this->setAttributSizes($validators['Sizes']);
        }
        if (isset($validators['Required'])) {
            $this->setAttributRequired($validators['Required']);
        }
    }
    
    /**
     * Recoit une entité et valide ses attributs avec les filtres et tailles dispo.
     * @param \Phaln\AbstractEntity $entity Entité non validée
     * @return \Phaln\AbstractEntity Entité validée
     */
    public function validateEntity(AbstractEntity $entity)
    {
        //  Récupère la liste des attributs de l'entité
        $attributs = $entity->__toArray();

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attributs as $name => $val) {
            $tmp = null;
        
            //  S'il est null et obligatoire, alors exception
            if (isset($this->attributRequired[$name]) && 'REQUIRED' === strtoupper($this->attributRequired[$name]) && is_null($val)) {
                throw new \Phaln\Exceptions\ValidatorException("Champ '$name' obligatoire.");
            }
        
            if (isset($this->attributFilters[$name]) && !is_null($val)) {
                //  L'attribut est du bon type
                $tmp = filter_var($val, $this->attributFilters[$name], FILTER_NULL_ON_FAILURE);
                //$tmp = ($tmp !== false) ? $tmp : null;
                if (is_null($tmp)) {
                    throw new \Phaln\Exceptions\ValidatorException("Champ '$name' filtrage incorrect.");
                }
            }
        
            // Tronque à la taille max.
            if (isset($this->attributSizes[$name]) && !is_null($tmp)) {
                $tmp = ($temp = substr($tmp, 0, $this->attributSizes[$name])) ? $temp : $tmp;
            }
                
            //  Affecte l'attribut
	    dump_var($tmp, DEBUG_PHALN, "tmp affecté à $name");
            $mutateur = 'set' . $name;
            $entity->$mutateur($tmp);
        }

	dump_var($entity, DEBUG_PHALN, "Entité retournée par validator");
        return $entity;
    }

    /**
     * Fabrique une entitée validée selon les tableaux définis en attribut.
     * @param string $className le nom de la classe entité, doit hériter de AbstractEntity
     * @param array $datas  le tableau des attributs de l'entité
     * @return \Phaln\AbstractEntity|null L'entité validée
     * @throws ValidatorException en cas de pb de création de l'entité
     */
    public function validEntityFactory(string $className, ?array $datas=null) : ?AbstractEntity
    {
	try {
	    return $this->validateEntity(new $className($datas)); 
	} catch (Exception $ex) {
	    throw new ValidatorException("Création $className validée impossible");
	}
    }
    
    /**
     * Vous pouvez surcharger cette méthode pour adapter votre manière de créer et de valider vos entités.
     * Permet de standardiser les appels.
     * @param array $datas  le tableau des attributs de l'entité
     * @return \Phaln\AbstractEntity|null L'entité validée
     */
    abstract public function createValidEntity(?array $datas) : ?AbstractEntity;
}
